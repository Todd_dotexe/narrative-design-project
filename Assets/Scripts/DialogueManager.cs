﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public Text nameText;
    public Text dialogueText;

    public Animator animator;

    private Queue<string> sentences;
    void Start()
    {
        sentences = new Queue<string>();
    }


    public void StartDialogue (Dialogue dialogue)
    {
        animator.SetBool("IsOpen", true);

        nameText.text = dialogue.name;

        sentences.Clear();

        Cursor.lockState = CursorLockMode.Confined;

        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }
        DisplayNextSentence();
    }
   
    public void DisplayNextSentence ()
    {
        
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }
            string sentence = sentences.Dequeue();
            dialogueText.text = sentence;

    }
    void EndDialogue()
    {
        animator.SetBool("IsOpen", false);
        Cursor.lockState = CursorLockMode.Locked;
    }
}
