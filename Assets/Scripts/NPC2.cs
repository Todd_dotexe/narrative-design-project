﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC2 : MonoBehaviour
{
    public Dialogue dialogue;
    public GameObject cam;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (Physics.Raycast(cam.transform.position, cam.gameObject.transform.forward, out var hit, 2f))
            {
                if (hit.collider.gameObject.tag == "NPC2")
                {
                    TriggerDialogue();
                }
            }
        }
    }
    public void TriggerDialogue()
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
    }
}
