﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine;

public class CoinPouch : MonoBehaviour
{
    public GameObject cam;

    public float MoneyBag = 0;
    public Text MoneyDisplay;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (Physics.Raycast(cam.transform.position, cam.gameObject.transform.forward, out var hit, 2f))
            {
                if (hit.collider.gameObject.tag == "Money")
                {
                    Destroy(hit.collider.gameObject);
                    MoneyBag++;
                }
            }
        }
    }
}
