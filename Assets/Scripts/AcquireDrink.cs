﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcquireDrink : MonoBehaviour
{
    public GameObject cam;

    public GameObject BarDrink;
    public GameObject DrinkFull;
    public GameObject DrinkHalf;
    public int DrinkState;

    void Start()
    {
        DrinkState = 0;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (Physics.Raycast(cam.transform.position, cam.gameObject.transform.forward, out var hit, 2f))
            {
                if (hit.collider.gameObject.tag == "Drink")
                {
                    Destroy(hit.collider.gameObject);
                    DrinkState = 2;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            DrinkState--;
        }
        if (DrinkState == 2)
        {
            DrinkFull.SetActive(true);
        }
        if (DrinkState == 1)
        {
            DrinkFull.SetActive(false);
            DrinkHalf.SetActive(true);
        }
        if (DrinkState == 0)
        {
            DrinkHalf.SetActive(false);
        }

    }
}
